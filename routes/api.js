const express = require('express');
const router = express.Router();
const Ninja = require('../models/ninja');

// const moment = require('moment-timezone');
// let time = moment().tz('Africa/Accra').format("dddd, MMMM Do YYYY, h:mm:ss a");

// get a list of ninjas from the db
router.get('/ninjas', function (req, res, next) {
    // Ninja.findById(req.params.id, function (err, doc) {
    //     if (err) next(err);
    //     res.status(200).send(doc);
    // });

    Ninja.find({}, function (err, docs) {
        if (err) next(err);
        res.status(200).send(docs);
    });

    // Ninja.geoNear(
    //     { type: 'Point', coordinates: [parseFloat(req.query.lng), parseFloat(req.query.lat)] },
    //     { maxDistance: 100000, spherical: true }
    // ).then(function (ninjas) {
    //     res.send(ninjas);
    // }).catch(next);
});

// add a new ninja to the db
router.post('/ninjas', function (req, res, next) {
    Ninja.create(req.body, function (err, doc) {
        if (err) next(err);
        res.status(201).send(doc);
    });
});

// update a ninja in the db
router.put('/ninjas/:id', function (req, res, next) {
    Ninja.findByIdAndUpdate({ _id: req.params.id }, req.body, function (err, doc) {
        if (err) next(err);
        res.status(200).send(doc);

        //To return updated doc instead of previous doc
        // Ninja.findOne({ _id: req.params.id }, function (err, doc) {
        //     if (err) next(err);
        //     res.status(200).send(doc);
        // });
    })
});

// delete a ninja from the db
router.delete('/ninjas/:id', function (req, res, next) {
    Ninja.findByIdAndRemove({ _id: req.params.id }, function (err, doc) {
        if (err) next(err);
        res.status(200).send(doc);
    });
});

module.exports = router;
