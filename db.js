let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
let DATABASE_NAME = 'Databug';

//Changing these values will not be enough its only to highlight key things you should change
//Copy your Atlas connection string using a version 2.2.12 if you have issues with newer versions

//open your cmd(I use cmdr) and type " set AtlasPassword='yoursecretkey' " OR BETTER YET USE A .ENV FILE
//you can always view the set value in cmd by typing "node" hit enter then type "process.env.AtlasPassword"

//let CONNSTRING = `mongodb://localhost/${DATABASE_NAME}`; //if local DB
// let CONNSTRING = process.env.DBSTRING; // gets CONNECTION STRING from env variable;
let CONNSTRING = `enter yor connection string here`;

mongoose.connect(`${CONNSTRING}`, { useMongoClient: true }, (error, client) => {
  if (error) {
    throw error;
  }
  //default collection name is users
  console.log("Connected to DB: " + DATABASE_NAME + " in Mongodb !");

});
