const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');

const db = require('./db');

// set up express app
const app = express();

//helmet helps you secure your Express apps by setting various HTTP headers.
app.use(helmet())

//enable cors
app.use(cors());

//set up static files
app.use(express.static('public'));

// use body-parser middleware
app.use(bodyParser.json());

// initialize routes
app.use('/api', require('./routes/api'));

// error handling middleware
app.use(function (err, req, res, next) {
    console.log(err); // to see properties of message in our console
    res.status(422).send({ error: err.message });
});

// listen for requests
let port = process.env.port || 4000;
app.listen(port, function () {
    console.log(`Now listening for requests on port: ${port} - http://localhost:${port}/`);
});
